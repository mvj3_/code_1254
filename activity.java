package yyl.fonts;

import Android.app.Activity;
import Android.graphics.Typeface;
import Android.os.Bundle;
import Android.widget.TextView;

public class FontsActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        //得到TextView控件对象
        TextView textView = (TextView)findViewById(R.id.custom);

        //将字体文件保存在assets/fonts/目录下，www.linuxidc.com创建Typeface对象
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/HandmadeTypewriter.ttf");

        //应用字体
        textView.setTypeface(typeFace);
    }
}